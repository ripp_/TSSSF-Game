window.C = {
    grid:{
        margin: 10,
        cardSize: 150,
    },direction:{
        right: 0,
        up: 1,
        left: 2,
        down: 3,
        none: -1
    },hand:{
        widthInCards: 3,
    },canvas:{
        widthInCards: 5,
        heightInCards: 3
    },colours:{
        cards:{
            pony:"#4b2a83",
            ship:"#c3136b",
            goal:"#3d2360",
            start:"#3c3538",
        },
        races:{
            earth:"#d89728",
            unicorn:"#5ebb46",
            pegasus:"#0cb9b5",
            alicorn:"#774d9f",
        },
        genders:{
            male:"#0b5493",
            female:"#e00e77",
            malefemale:"#9ab0b8",
        },
        hourglass:"#b61f24",
        effects:{
            replace:"#D50000",
            swap:"#C51162",
            draw:"#00C853",
            newGoal:"#0091EA",
            search:"#FFAB00",
            copy:"#3E2723"
        }
    }
}
