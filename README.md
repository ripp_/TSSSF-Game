# TSSSF-Game
(The name is basic because otherwise I spend weeks thinking of one)

## Goal
To create a web version of Twilight Sparkle's Secret Shipfic Folder, with on line multilayer support

## Protocol

### Client -> Server

#### `auth`
Sent immediately after connecting.
 * `authType`: The method used to auth, currently not checked, eventually will be different login methods
 * `key`: the passkey used to auth, or NONE if creating a new user
 * `nick`: the nickname to use, ignored if `key` is passed and valid

### Bi Directional

#### `chat`
A basic chat message
 * `client` *(From server only)*: The user who sent the message
 * `msg`: The chat message

#### `create`
Creates a new game room (only valid from lobby). After being created the creator will be moved into the new room.
 * `owner` *(From server only)*: The creator of the room
 * `room` *(From server only)*: The room's info
 * `name`: The name of the room
 * `password`: *From client:* The password for the room or `None`, *From Server:* If the room is passworded

### Server -> Client

#### `join`
Indicates a client has joined the room
 * `client`: The user who has joined.
 * `room`: The room the user has joined.

#### `leave`
Indicates a client has left the room
 * `client`: The user who has left
 * `room`: The room the user has left

#### `disconnect`
Indicates a client has disconnected from the room
 * `client`: The user who has disconnected
 * `room`: The room the user has disconnected from

#### `rooms`
Provides a list of open game rooms.
 * `rooms`: A list of game rooms.

#### `destory`
Indicates a room has been destroyed, game rooms are automatically destroyed when empty
 * `room`: The removed room
