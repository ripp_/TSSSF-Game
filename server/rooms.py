import smartJson as json
import logging
import os

_log = logging.getLogger(__name__)
#_log.addHandler(logging.StreamHandler())
#_log.setLevel(int(os.getenv("DBLEVEL",logging.INFO)))

def Singleton(cls):
    cls.IS_SINGLETON = True
    cls.name = cls.__name__
    return cls()

class Room(json.BasicJsonEncodable):
    IS_SINGLETON = False
    _jsonProperties = set(("types","name"))

    @property
    def types(self):
        return [p.__name__ for p in self.__class__.__mro__ if issubclass(p,Room)]

    def __init__(self):
        self.clients = set()

    def __str__(self):
        return "Room.{}".format(self.__class__.__name__)

    def addClient(self,client):
        """Removes a client from their current room then adds them to this one."""
        if client.room is not None:
            client.room.clients.remove(client)
            client.room.handleLeave(client)
        self.clients.add(client)
        client.room = self
        self.handleAdd(client)

    def disconnectClient(self,client):
        """Removes the given client from the room then calls `handleClose` for them"""
        self.clients.remove(client)
        self.handleClose(client)

    def broadcast(self,**payload):
        """Sends the given message (given as kwargs) to all connected clients"""
        jMsg = json.dumps(payload)
        for c in self.clients:
            c.sendRaw(jMsg)

    def handleAdd(self,client):
        """Called after a client has been added to the room"""
        pass

    def handleLeave(self,client):
        """Called after a client has been removed from the room"""
        pass

    def handleClose(self,client):
        """Called after a client has diconnected and been removed from the room"""
        pass

class ChatRoom(Room):
    def on_chat(self,client,msg):
        _log.info("%s - %s : %s",self,client.nick,msg)
        self.broadcast(type="chat",msg=msg,client=client)

    def handleAdd(self,client):
        _log.info("%s - %s : has joined",self,client.nick)
        self.broadcast(type="join",client=client,room=self)

    def handleLeave(self,client):
        _log.info("%s - %s : has left",self,client.nick)
        self.broadcast(type="leave",client=client,room=self)

    def handleClose(self,client):
        _log.info("%s - %s : has disconnected",self,client.nick)
        self.broadcast(type="disconnect",client=client,room=self)


@Singleton
class Limbo(Room):
    def handleAdd(self,client):
        client.hasAuthed = False
        client.nick = None
        _log.info("{} has entered Limbo".format(client.address[1]))
        client.send(type="connected",client=client)

    def on_auth(self,client,authType,key=None,nick=None):
        client.hasAuthed = True
        client.nick = nick
        Lobby.addClient(client)

@Singleton
class Lobby(ChatRoom):
    def __init__(self):
        ChatRoom.__init__(self)
        self.games = {}

    def destroyRoom(self,room):
        for c in list(room.clients):
            self.addClient(c)
        self.broadcast(type="destroy",room=room)
        del self.games[id(room)]

    def handleAdd(self,client):
        ChatRoom.handleAdd(self,client)
        client.send(type="rooms",rooms=self.games.values())

    def on_create(self,client,name,password=None):
        room = GameRoom(client,name,password)
        self.games[id(room)] = room
        self.broadcast(type="create",owner=client,room=room,password=bool(password))
        room.addClient(client)

    def on_join(self,client,room):
        #TODO check room is accepting effect
        self.games[room].addClient(client)

class GameRoom(ChatRoom):
    _jsonProperties = set(("types","name","owner","id"))

    def __init__(self,owner,name,password):
        ChatRoom.__init__(self)
        self.owner = owner
        self.name = name
        self.password = password
        self.options = {
            "spectators":False
        }

    def handleAdd(self,client):
        ChatRoom.handleAdd(self,client)
        client.send(type="options",options=self.options)

    def handleLeave(self,client):
        ChatRoom.handleLeave(self,client)
        if not self.clients:
            del Lobby.games[id(self)]

    def handleClose(self,client):
        ChatRoom.handleClose(self,client)
        if not self.clients:
            del Lobby.games[id(self)]

    def on_options(self,client,**options):
        _log.info(options)
        if not self.assertOwner(client,"You are not allowed to adjust the options"):
            return
        for k,v in options.items():
            if k in self.options:
                if type(v) is type(self.options[k]):
                    self.options[k] = v
        _log.info(self.options)
        self.broadcast(type="options",client=client,options=self.options)

    def assertOwner(self,client,msg=None):
        if client is not self.owner:
            if msg:
                client.send(type="error",msg=msg)
            return False
        return True
