import json,traceback
import logging
import os

_log = logging.getLogger(__name__)
#_log.addHandler(logging.StreamHandler())
#_log.setLevel(int(os.getenv("DBLEVEL",logging.INFO)))

class JsonEncodable(object):
    def _toJsonDict(self):
        raise NotImplementedError

    @classmethod
    def _fromJsonDict(cls,dict):
        return NotImplemented

class BasicJsonEncodable(JsonEncodable):
    _jsonProperties = set()
    @property
    def id(self):
        return id(self)

    def _toJsonDict(self):
        return dict({k:getattr(self,k) for k in self._jsonProperties})

    @classmethod
    def _fromJsonDict(cls,data):
        if set(data.keys()) == cls._jsonProperties:
            rtn = cls()
            for k,v in data.iteritems():
                setattr(rtn,k,v)
            return rtn
        else:
            return NotImplemented

def yeildDecendents(cls):
    for child in cls.__subclasses__():
        yield child
        for grandchild in yeildDecendents(child):
            yield grandchild

class Decoder(json.JSONDecoder):
    def object_hook(self,jsonDict):
        for dCls in yeildDecendents(JsonEncodable):
            r = dCls._fromJsonDict(jsonDict)
            if r is not NotImplemented:
                return r
        return json.JSONDecoder(self,jsonDict)

class Encoder(json.JSONEncoder):
    def default(self,o):
        causedBy = []
        try:
            return o._toJsonDict()
        except Exception as e:
            causedBy.append(e)
            _log.debug("%s raised whilst trying _toJsonDict on %s, trying default",e,o)
        try:
            return json.JSONEncoder.default(self,o)
        except TypeError as e:
            causedBy.append(e)
            _log.debug("%s raised whilst trying default on %s, testing for iterator",e,o)
        try:
            return [self.default(p) for p in o]
        except Exception as e:
            _log.error("%s raised whilst trying to convert %s to json, possibly caused by:\n\t %s",e,o,"\n\t".join(causedBy))

def dumps(*args,**kwargs):
    return json.dumps(*args,cls=Encoder,**kwargs)

def loads(*args,**kwargs):
    #return json.loads(*args,cls=Decoder,**kwargs)
    return json.loads(*args,**kwargs)
