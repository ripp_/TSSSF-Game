#!/usr/bin/python3

from rooms import *

import websockets
import smartJson as json
import traceback
import logging
import asyncio
import os

HANDLERS = set()
_log = logging.getLogger(__name__)


@asyncio.coroutine
def handler(websocket,uri):
    c = Client(websocket)
    c.handleConnected()
    HANDLERS.add(c)
    while 1:
        r = yield from websocket.recv()
        c.handleMessage(r)
        for h in HANDLERS:
            try:
                yield from h.sendAll()
            except websockets.exceptions.ConnectionClosed as e:
                _log.debug("Client disconnected whilst trying to send message %s")
    HANDLERS.remove(c)
    c.handleClose()

class Client(json.BasicJsonEncodable):
    _jsonProperties = set(("nick","cid"))
    
    def __init__(self,websocket):
        self.websocket = websocket
        self.toSend = []
    
    def __str__(self):
        return "Client-{}".format(self.address)
    
    def sendAll(self):
        while self.toSend:
            yield from self.websocket.send(self.toSend.pop(0))
    
    @property
    def address(self):
        return self.websocket.remote_address

    def handleConnected(self):
        self.room = None
        self.cid = self.address[1]
        _log.info("client connected %s",self)
        Limbo.addClient(self)

    def handleClose(self):
        if self.room is not None:
            self.room.disconnectClient(self)
        _log.info("client closed %s",self)

    def handleMessage(self,data):
        handler = None
        try:
            msg = json.loads(data)
        except ValueError as e:
            _log.warn("Cannot decode data - bad json %s",data)
            return

        try:
            handler = getattr(self.room,"on_"+msg["type"])
        except KeyError:
            _log.warn("No type in json")
            return
        except AttributeError as e:
            _log.warn("Unknown message type: %s",msg["type"])
            return
        except TypeError as e:
            _log.warn("Bad paramaters for method %s %s",msg["type"],msg)
            return

        del msg["type"]
        msg["client"] = self

        handler(**msg)

    def sendRaw(self,message):
        #message = message.encode("utf-8")
        self.toSend.append(message)

    def send(self,**payload):
        jMsg = json.dumps(payload)
        self.sendRaw(jMsg)

if __name__ == "__main__":
    import sys
    
    logging.basicConfig(level=int(os.getenv("DBLEVEL",logging.INFO)))
    def addColourToEmitter(fn):
        def format(self,record):
            cols = ["\x1b[0m","\x1b[35m","\x1b[32m","\x1b[33m","\x1b[31m"]
            rLevel = record.levelno//10
            if rLevel < 0: rLevel = 0
            elif rLevel >= len(cols): rLevel = len(cols)
            record.msg = cols[rLevel] + record.msg + "\x1b[0m"
            return fn(self,record)
        return format
    logging.StreamHandler.emit = addColourToEmitter(logging.StreamHandler.emit)        
        
    port = 8000 if len(sys.argv)<3 else sys.argv[2]
    host = "0.0.0.0" if len(sys.argv)<2 else sys.argv[1]
        
    print("Running on {}:{}".format(host,port))
    
    socketServer = websockets.serve(handler,host,port)
    asyncio.get_event_loop().run_until_complete(socketServer)
    asyncio.get_event_loop().run_forever()
